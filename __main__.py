import testing

def run_tests(*args, **kwargs):
    testing.main(*args, **kwargs)
    
def run_benchmark(*args, **kwargs):
    testing.run_benchmark(*args, **kwargs)
