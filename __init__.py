import pkgutil, importlib, difflib # standard lib packages
import sys, subprocess, shlex, fcntl, os, time, errno # standard lib packages
import logging; log = logging.getLogger(__name__)
import traceback

EXPECTED_FILE_PREFIX = "file://"

##############################################3

def all_subpackages(pkg_prefix, verbose, what_to_run, parent, prefix=""):
    if not pkg_prefix.endswith("."): pkg_prefix += "."
    to_skip = []

    try:
        path = parent.__path__ # raises AttributeError if module
        to_browse = pkgutil.walk_packages(parent.__path__, prefix=pkg_prefix, onerror=lambda x : None)
    except AttributeError:
        to_browse = [("not used", parent.__name__, False)]
        
    for _, modname, ispkg in to_browse:
        if ".testing" not in modname and "_testing" not in modname and not ispkg:
            continue
        
        mod = importlib.import_module(modname)
        
        path = mod.__path__[0] if ispkg else mod.__file__
        do_skip = False
        for skip in to_skip:
            if path.startswith(skip):
                do_skip = True
                break
        if do_skip:
            continue
            
        if os.path.islink(path):
            log.info("Skipping symbolic link to {}".format(mod.__path__[0]))
            # might be better to check if it is in the path first ...
            if ispkg:
                to_skip.append(mod.__path__[0])
            continue

        for what in what_to_run:
            if not hasattr(mod, what): continue
            try:
                run_module(verbose, modname, mod, getattr(mod, what))
            except Exception as e:
                log.warn("{} failed {}".format(modname, e))
                traceback.print_exc()
                
def main(pkg_prefix, module=None,  benchmark=True, test=True, verbose=False, help=False):
    what_to_run = []

    if help:
        print(""" Options:
    pkg_prefix: run only tests/benchmarks inside this package (mandatory)

    benchmark: If false, do not run benchmark
    tests: If false, do not run tests
    verbose: If true, print some more details
    help: Prints this help.
""")
        return

    if module is None:
        module = importlib.import_module(pkg_prefix)
    
    if test: what_to_run.append("test")
    if benchmark: what_to_run.append("benchmark")
    
    all_subpackages(pkg_prefix, verbose, what_to_run, module)

def run_benchmark(pkg_prefix, module=None, verbose=False):
    GDB_Instance.CSV_mode = True

    all_subpackages(pkg_prefix, verbose, ["benchmark"], module)
    
##############################################3
class CompilationError(Exception): pass
class TerminatedError(Exception): pass

class CSource:
    def __init__(self, srcname):
        self.srcname = srcname
        self.path, _, self.binname = srcname.rpartition("/")
        self.binname = self.binname.rpartition(".")[0] 

        if not self.path: self.path = "."
        self._package = None
        self.target_dir = None
        
    @property
    def package(self):
        return self._package
        
    @package.setter
    def package(self, package):
        self._package = package if hasattr(package, "__path__") else \
            importlib.import_module(package.__package__)
        
        self.src_dir = "{pkgpath}/{path}".format(pkgpath=self.package.__path__[0],
                                                 path=self.path)
        self.target_dir = "{}/__binaries__/".format(self.src_dir)
        
    def compile(self):
        assert self.target_dir is not None
        
        mkdir_p(self.target_dir)
        bin = self.binary.replace(self.src_dir, "")
        if bin.startswith("/"): bin = bin[1:]
        cmd = "make -C {path} {bin} ".format(path=self.src_dir, bin=bin)

        if 0 != os.system(cmd+" >/dev/null"):
            raise CompilationError("Could not make {} ({})".format(self.binary, cmd))

    @property
    def binary(self):
        assert self.target_dir is not None

        return self.target_dir + self.binname

class FinishException(Exception):
    pass

def run_module(verbose, modname, mod, do_that):
    def run_once():
        try:
            gdb_instance = GDB_Instance(verbose, mod)
            do_that(gdb_instance)
            return True
        
        except TerminatedError as e:
            print(e)
        except CompilationError as e:
            print("ERROR: Could not compile ...")
            print("ERROR: {}".format(e))

            
    if not GDB_Instance.CSV_mode:
        title = "### "+modname+" ###"
        print("#" * len(title))
        print(title)
        print("#" * len(title))

        try:
            run_once()
        except FinishException:
            pass
    else:
        print("# "+modname)
        GDB_Instance.CSV_header = True
        run_once()
        print("iteration") # finish the line
        
        GDB_Instance.CSV_header = False
        try:
            for ite in range(GDB_Instance.CSV_REPEAT):
                if not run_once():
                    break
                print(ite) # finish the line
        except FinishException:
            pass
        
def expected_file(filename):
    return EXPECTED_FILE_PREFIX + filename

class GDB_Output(list):
    def __init__(self, command, out, err, correct):
        list.__init__(self, [command, out, err, correct])
        self.command = command
        self.out = out
        self.err = err
        self.correct = correct
        
    def print_all(self):
        if self.err:
            print(self.err)
        if self.out:
            print(self.out)
            
class GDB_Instance:
    PROMPT = "(gdb) "
    TIMEOUT = 20 # * 0.1 s = 2s before checking for exceptions
    
    CSV_mode = False
    CSV_header = False
    CSV_REPEAT = 10
    interactive = False
    
    def __init__(self, verbose, package):
        self.verbose = verbose
        
        self._gdb = None
        self.package = package
        self._fout = None
        self._fcmd = None
        self.src = None
        
    def start(self, source, init_hook=None):
        self.src = source
        self.init_hook = init_hook
        
        source.package = self.package
        source.compile()

        command_line = "gdb -nx"
        self._gdb = subprocess.Popen(shlex.split(command_line),
                                     stdin=subprocess.PIPE,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
        # Make stderr and stdout non-blocking.
        for outfile in (self._gdb.stdout, self._gdb.stderr):
            if outfile is not None:
                fd = outfile.fileno()
                fl = fcntl.fcntl(fd, fcntl.F_GETFL)
                fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

        self._fout = open("/tmp/gdb.out", "wb")
        self._fcmd = open("/tmp/gdb.cmd", "wb")
        
        out, err = self._read_til_prompt(None)

        self.execute("set confirm off")

        if init_hook:
            init_hook(self)
            
        self.execute("file {}".format(source.binary))
        
    def check_results(self, command, expected, results):
        try:
            return expected.search(results) is not None
        except AttributeError: # not a regex
            pass

        
        if expected.startswith(EXPECTED_FILE_PREFIX):
            expected_filename = expected[len(EXPECTED_FILE_PREFIX):]
            with open("{}/{}".format(self.src.src_dir, expected_filename)) as expected_file:
                expected = "".join(expected_file.readlines())
        
        return expected in results
        
        
    def execute_many(self, commands, *args, **kwargs):
        if not isinstance(commands, list):
            commands = [commands]

        return [self.execute(a_command, *args, **kwargs) for a_command in commands]


    def test(self, success, details=""):
        if success:
            self.test_success(details)
        else:
            self.test_fail(details)
            
    def test_fail(self, details=""):
        print("FAILED {}{}".format(self.last_cmd, " ({})".format(details) if details else ""))
        import pdb;pdb.set_trace()
        pass
    
    def test_success(self, details=""):
        if self.verbose:
            print("PASSED")
    
    def test_expected_fail(self, details=""):
        print("EXPECTED FAILURE {}{}".format(self.last_cmd, "({})".format(details) if details else ""))
    
    def execute(self, command, expected_results=None, expected_stderr=None, may_fail=False, check_stdout=None, check_stderr=None):
        if GDB_Instance.CSV_header: return GDB_Output(command, "", "", True)

        if not self._gdb:
            raise ProcessLookupError("gdb not running")
        
        assert not isinstance(command, list)

        self.last_cmd = command
        self.last_err = None
        self.last_out = None
        
        self.last_out, self.last_err = self._read_til_prompt(command)

        if check_stdout is None:
            check_stdout = self.check_results
        if check_stderr is None:
            check_stderr = self.check_results
            
        correct = all([
            not expected_results or check_stdout(command, expected_results, self.last_out),
            not expected_stderr or check_stderr(command, expected_results, self.last_err)
            ])
        
        if not (expected_results or expected_stderr):
            pass
        elif correct:
            self.test_success()
        elif may_fail:
            self.test_expected_fail()
            correct = None
        else:
            self.test_fail()

        if not correct and not may_fail and self.verbose:
            for results, name in (self.last_out, "stdout"), (self.last_err, "stderr"):
                if not results: continue
                for line in difflib.context_diff(expected_results.split("\n"), results.split("\n"),
                                                 fromfile='Expected results', tofile='Actual results ({})'.format(name)):
                    print(line)
            if not (out or err):
                print("Expected results: (got nothing)")
                print(expected_results)
        #print(levenshtein_distance(expected, results))
        if self.interactive: print("")
        
        return GDB_Output(command, self.last_out, self.last_err, correct)

    def set_title(self, title):
        if GDB_Instance.CSV_header:
            print('"{}"'.format(title), end=" ")
        elif GDB_Instance.CSV_mode:
            pass
        else:
            print(title)
            print("-" * len(title))
            
    def save_py_value(self, name):
        if GDB_Instance.CSV_header: return
        
        output = self.execute("python print({})".format(name))

        value = output.out[:-1]
        if GDB_Instance.CSV_mode:
            print(value, end=", ", flush=True)
        else:
            output.print_all()

        return value
    
    def save_value(self, name, format, unit):
        if GDB_Instance.CSV_header: return
        
        output = self.execute('printf "{format}{unit}\\n", {name}'.format(
                name=name, format=format, unit=unit))

        value = output.out[:-1]
        if GDB_Instance.CSV_mode:
            print(value, end=", ", flush=True)
        else:
            output.print_all()

        return value
    
    def _read_til_prompt(self, command):
        if command is not None:
            if not command.endswith("\n"): command = command + "\n"
            
            for to_file in self._gdb.stdin, self._fout, self._fcmd:
                if to_file is self._fout:
                    to_file.write(GDB_Instance.PROMPT.encode('ascii'))
                    
                to_file.write(command.encode('ascii'))
                to_file.flush()
            
        out_buffer = ''
        err_buffer = ''

        def read_utf8_buffer(buff):
            bs = buff.read()
            return bs.decode("utf-8") if bs else ""

        timeout = GDB_Instance.TIMEOUT
        while not out_buffer.endswith(GDB_Instance.PROMPT):
            try:
                self._gdb.poll()
                if self._gdb.returncode is not None:
                    raise TerminatedError(self._gdb.returncode)
                
                out = read_utf8_buffer(self._gdb.stdout)
                out_buffer += out
                if self.interactive: print(out, end="")
                
                err = read_utf8_buffer(self._gdb.stderr)
                err_buffer += err
                if self.interactive: print(err, end="", file=sys.stderr)
                
                if not err and not out and timeout > 0:
                    timeout -= 1
            except IOError:
                time.sleep(0.1)
            except KeyboardInterrupt:
                import pdb;pdb.set_trace()
                print(out_buffer)

            if timeout <= 0 and  "(Pdb)" in out_buffer:
                print(err_buffer)
                print(out_buffer, end="")
                print("FORCE QUITTING PDB")
                
                self._gdb.stdin.write("import os;os._exit(0)\n".encode('ascii'))
                raise TerminatedError(None)
        
        out_buffer = out_buffer[:-(len(GDB_Instance.PROMPT))].strip()
        self._gdb.stderr.flush()
        while True:
            err = read_utf8_buffer(self._gdb.stderr)
            if not len(err): break
            err_buffer += err

        if out_buffer:
            if not out_buffer[-1] == '\n': out_buffer += '\n'
            self._fout.write("--- OUT ---\n".encode("utf-8"))
            self._fout.write(out_buffer.encode("utf-8"))
        if err_buffer:
            if not err_buffer[-1] == '\n': out_buffer += '\n'
            self._fout.write("--- ERR ---\n".encode("utf-8"))
            self._fout.write(err_buffer.encode("utf-8"))
        if out_buffer or err_buffer:
            self._fout.write("--- *** ---\n".encode("utf-8"))
        self._fout.flush()
        
        return out_buffer, err_buffer

    def reset(self, hard=False):
        self.execute("delete")
        self.execute("kill")
        
        if hard:
            self.quit()
            self.start(self.src, self.init_hook)
            
    def sync_outputs(self):

        print("# Syncing ...")
        i = 1
        what = "nothing to start with"
        while True:
            self._gdb.stderr.flush()
            
            buff = ""
            while True:
                b = self._gdb.stderr.read()
                if b is None: break
                buff += b.decode("utf-8")
                
            if what in buff: break
                
            what = "error{}".format(i)
            res = self.execute(what).err
            if what in res: break

            time.sleep(0.3)
            i += 1
            
    def quit(self):
        if not self._gdb:
            return
        
        try:
            self.execute("kill")
            self.execute("quit")
            
            self._gdb.terminate()
            self._gdb.kill()
            
        except TerminatedError as e:
            return e.args[0]
        finally:
            if self._fout:
                self._fout.close()
            if self._fcmd:
                self._fcmd.close()

    def get_location(self, text, src_fname=None):
        if src_fname is None:
            src_fname = "/".join((self.src.src_dir, self.src.srcname))
            
        with open(src_fname) as src_f:
            for i, line in enumerate(src_f.readlines()):
                if text in line:
                    return i

##############################################3

def levenshtein_distance(first, second):
    """Find the Levenshtein distance between two strings."""
    if len(first) > len(second):
        first, second = second, first
    if len(second) == 0:
        return len(first)
    first_length = len(first) + 1
    second_length = len(second) + 1
    distance_matrix = [[0] * second_length for x in range(first_length)]
    for i in range(first_length):
       distance_matrix[i][0] = i
    for j in range(second_length):
       distance_matrix[0][j]=j
    for i in range(1, first_length):
        for j in range(1, second_length):
            deletion = distance_matrix[i-1][j] + 1
            insertion = distance_matrix[i][j-1] + 1
            substitution = distance_matrix[i-1][j-1]
            if first[i-1] != second[j-1]:
                substitution += 1
            distance_matrix[i][j] = min(insertion, deletion, substitution)
    return distance_matrix[first_length-1][second_length-1]

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST: pass
        else: raise
        
##############################################3

if __name__ == "__main__":
    main()

    
