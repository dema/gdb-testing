from gdb_tester import CSource

gdb = None

def run(what, run="run"):
    gdb.set_title(what)
    gdb.execute("break finish_data_ready")
    gdb.execute(run)
    gdb.execute("up")

    return gdb.save_value("us_busy_once", "%1.f", "us")

def version():
    gdb.set_title("version")
    
    print(gdb.execute('show version')[1].split("\n")[0])
    print(gdb.execute('pi print("Python "+str(sys.version))')[1][:-1].replace("\n", " --- "))
    print("")
    
def nominal_time():
    run("Nominal time")

def gdb_watchpoint():
    gdb.execute("break benchmark")
    gdb.execute("run")
    gdb.execute("print &i")
    gdb.execute("watch *$1")
    gdb.execute("""command
silent
continue
end""")
    gdb.execute("py [b for b in gdb.breakpoints() if b.location == 'benchmark'][0].delete()")
    
    run("HW Watchpoint command", run="continue")
    
def gdb_breakpoint():
    gdb.execute("break action")
    gdb.execute("""command
silent
continue
end""")
    run("Breakpoint command")

PARAMETERS = [
    'int(gdb.parse_and_eval("it"))',
    'int(gdb.newest_frame().older().read_var("i"))'
    ]

PY_ON_BREAKPOINT = """python
class TestBP(gdb.Breakpoint):
  def __init__(self):
    gdb.Breakpoint.__init__(self, "action")
    self.silent=True

  def stop(self):
    {on_breakpoint}
    return False

TestBP()
end
"""

def gdb_py_breakpoint_sleep_for_ref():
    SLEEP_TIME = 0.001
    gdb.execute(PY_ON_BREAKPOINT.format(on_breakpoint="import time;time.sleep({})".format(SLEEP_TIME)))
        
    run("Python sleep {}s on breakpoint for reference".format(SLEEP_TIME))

def gdb_py_breakpoint():
    gdb.execute(PY_ON_BREAKPOINT.format(on_breakpoint=""))
        
    run("Python breakpoint")
    
def gdb_py_breakpoint_parameter(params_choice):
    params = [par for i, par in enumerate(PARAMETERS) if params_choice & (1 << i)]
            
    def to_run():
        gdb.execute(PY_ON_BREAKPOINT.format(on_breakpoint="\n    ".join(params)))
        
        run("Py BP param {:02b}".format(params_choice)[2:])
        
    return to_run

ref_time_set_bp = None
def do_nothing():
    global ref_time_set_bp

    assert ref_time_set_bp is None
    ref_time_set_bp = set_breakpoints(nb_bp=0)
    ref_time_set_bp = float(ref_time_set_bp[:-2])
    
def set_breakpoints(nb_bp=1000):
    assert not (ref_time_set_bp is None and nb_bp != 0)
    try:
        import mcgdb.testing
    except ImportError:
        print("Cannot test breakpoint setting without mcgdb at the moment.")
        return
    
    mcgdb.testing.gdb__init_mcgdb(gdb)
    
    gdb.execute("""python
class VoidBP(gdb.Breakpoint):
  def __init__(self, addr):
    gdb.Breakpoint.__init__(self, "*{{}}".format(addr), internal=True)
    self.silent=True

class TestBP(mcgdb.capture.FunctionBreakpoint):
  def __init__(self):
    mcgdb.capture.FunctionBreakpoint.__init__(self, "action")

  def prepare_before (self):
    NB_BP = {np_bp}
    print("create breakpoints")
    start_addr = int(gdb.parse_and_eval("main").address)

    bps = []
    for addr in range(start_addr, start_addr+NB_BP):
       bps.append(VoidBP(addr))

    return (False, True, bps)

  def prepare_after(self, bps):
    print("delete all")
    for bp in bps:
      bp.delete()
    return False

TestBP()
end
""".format(np_bp=nb_bp))
    gdb.execute("start")
    gdb.execute("set var it_count = 10")

    exec_time = run("set {} breakpoints".format(nb_bp), run="continue")
    
    if nb_bp != 0:
        gdb.execute('pi bp_create_time = float(gdb.parse_and_eval("us_busy_once"))')
    

        gdb.execute('pi bp_create_time -= {}'.format(ref_time_set_bp))

        print(gdb.execute('pi print("Soit {:.2f}us".format(bp_create_time))')[1])
        
        gdb.execute("pi bp_create_time /= {}".format(nb_bp))

        print(gdb.execute('pi print("Soit {:.2f}us/bpt".format(bp_create_time))')[1])
        
    return exec_time

def benchmark(_gdb, what=None, at_init=None):
    global gdb
    gdb = _gdb

    #what = [version,  nominal_time, gdb_py_breakpoint, gdb_py_breakpoint_sleep_for_ref]
    what = list(map(lambda x: gdb_py_breakpoint_parameter(x), (0b0, 0b1, 0b10, 0b11)))
                
    if what is None:
        what = [version, do_nothing, set_breakpoints, nominal_time, gdb_breakpoint, gdb_watchpoint] + \
            list(map(lambda x: gdb_py_breakpoint_parameter(x), (0b0, 0b1, 0b10, 0b11)))
        
    gdb.start(CSource("../mcgdb_testing/benchmark.c"), at_init)
    for prepare_and_run in what:
        prepare_and_run()
        gdb.reset(hard=(at_init is not None))
        
    gdb.quit()




