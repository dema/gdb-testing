native_gdb = None
gdb = None

TEST_FINISH_BP = """python
class TestFctBP(mcgdb.capture.FunctionBreakpoint):
  do_finish = False

  def __init__(self):
    mcgdb.capture.FunctionBreakpoint.__init__(self, "action")

  def prepare_before (self):
    return (False, TestFctBP.do_finish, {})

  def prepare_after(self, data):
    return False
TestFctBP()
end
"""

def function_breakpoint():
    gdb.execute(TEST_FINISH_BP)
    native_gdb.run("Function Breakpoint")
    
def finish_breakpoint():
    gdb.execute(TEST_FINISH_BP)
    gdb.execute("python TestFctBP.do_finish = True")

    native_gdb.run("Finish Breakpoint")

def prepare(_gdb):
    gdb.execute("py import mcgdb")
    
def benchmark(_gdb):
    global native_gdb, gdb
    from ..gdb_testing import native_gdb
    import mcgdb.testing
    gdb = _gdb
    what = [function_breakpoint, finish_breakpoint]

    native_gdb.benchmark(_gdb, what, mcgdb.testing.gdb__init_mcgdb)
