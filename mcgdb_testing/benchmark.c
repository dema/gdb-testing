#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct timespec tbefore, tafter;


/***********/
// modified by debugging, do not change to macro.
int repeat = 2;

int it_count = 1000;
int us_sleep_time = 100;

/***********/

void finish_data_ready(void) {}

void finish(struct timeval x, struct timeval y) {
  
  static float it_total;
  static float ms_total_sleep;
  static float us_time, us_busy_once;
  {
    long nsec;
    
    time_t sec = tafter.tv_sec - tbefore.tv_sec;

    if (tafter.tv_nsec > tbefore.tv_nsec) {
        nsec = tafter.tv_nsec - tbefore.tv_nsec;
    } else {
        sec--;
        nsec = (1000000000L + tafter.tv_nsec) - tbefore.tv_nsec;
    }
    printf("Total time: %ld.%09ld\n", sec, nsec);
    it_total = repeat * it_count;
    
    us_time =  ((float)sec) * (1 / it_total * 1000000);
    us_time += ((float) nsec) / (it_total * 1000);

    us_busy_once = us_time - us_sleep_time;

    ms_total_sleep = (float)(us_sleep_time * it_total) / 1000000;
  }
  
  printf("Repeat: %d; Loop: %d; usleep %dus (one) %.2fms (total)\n",
         repeat, it_count, us_sleep_time, ms_total_sleep);
  printf("------------------\n");

  printf("Busy once : %1.fus\n", us_busy_once);

  finish_data_ready();
}

void action(int it) {
  usleep(us_sleep_time);
}

void benchmark(void) {
  static int i;
  
  for (i = 0; i < it_count; ++i) {
    action(i);
  }
}

int main(int argc, char** argv) {
    struct timeval before , after;
    int i;
    
    benchmark(); // warm-up

    clock_gettime(CLOCK_MONOTONIC_RAW, &tbefore);

    for (i = 0; i < repeat; ++i) {
      benchmark();
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &tafter);

    finish(before, after);
    
    return EXIT_SUCCESS;
}
