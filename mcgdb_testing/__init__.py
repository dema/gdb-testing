import importlib

def gdb__init_mcgdb(gdb):
    import mcgdb
    mcGDB_PATH = mcgdb.__path__[0]
    
    gdb.execute("python sys.path.append('{}/..')".format(mcGDB_PATH))
    gdb.execute("python import mcgdb; mcgdb.initialize()")

def run_tests(*args, **kwargs):
    import gdb_tester
    import mcgdb

    if "pkg_prefix" not in kwargs:
        kwargs["pkg_prefix"] = "mcgdb"
    else:
        kwargs["module"] = importlib.import_module(kwargs["pkg_prefix"])
        
    if "module" not in kwargs:
        kwargs["module"] = mcgdb
        
    gdb_tester.main(*args, **kwargs)
    
def run_benchmark(*args, **kwargs):
    import gdb_tester
    import mcgdb
        
    if "pkg_prefix" not in kwargs:
        kwargs["pkg_prefix"] = "mcgdb"
    else:
        kwargs["module"] = importlib.import_module(kwargs["pkg_prefix"])

    if "module" not in kwargs:
        kwargs["module"] = mcgdb
        
    gdb_tester.run_benchmark(*args, **kwargs)
