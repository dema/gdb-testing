from gdb_tester import *

gdb = None

def prepare_and_run(what):
    gdb.set_title(what.replace("do_", ""))

    gdb.execute("start")

    gdb.execute("set what_to_do = {}".format(what))
    
    gdb.execute("break finish_data_ready")
    gdb.execute("continue")
    gdb.execute("up")
    gdb.execute("python import mcgdb")
    
    gdb.save_value("us_busy_once", "%1.f", "us")    

def benchmark(_gdb):
    global gdb
    gdb = _gdb

    from . import benchmark
    
    gdb.start(CSource("benchmark.c"))
    for what in benchmark.WHAT_TO_RUN:
        gdb.reset(hard=True)
        prepare_and_run(what)
        
    gdb.quit()
