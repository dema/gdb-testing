from gdb_tester import *
import mcgdb

gdb = None

def prepare_and_run(what):
    gdb.set_title(what.replace("do_", "").replace("_", " "))
    
    gdb.execute("set env OMP_NUM_THREADS=2")

    
    gdb.execute("py import mcgdb; mcgdb.load_by_name('mcgdb-omp')")
    gdb.execute_many(["start"])
    
    gdb.execute("set var it_count=10")
    
    gdb.execute("set what_to_do = {}".format(what))
    gdb.execute("break finish_data_ready")
    gdb.execute("continue")
    gdb.execute("up")
    
    gdb.save_value("us_busy_once", "%1.f", "us")
    
WHAT_TO_RUN = [
    "do_critical",
    "do_master",
    "do_sections",
    "do_barrier",
    "do_single",
    "do_single_task"
    ]
    
def benchmark(_gdb):
    global gdb
    gdb = _gdb

    gdb.start(CSource("benchmark.c"), init_hook=mcgdb.testing.gdb__init_mcgdb)
    for what in WHAT_TO_RUN:
        gdb.reset(hard=True)
        prepare_and_run(what)
        
    gdb.quit()
