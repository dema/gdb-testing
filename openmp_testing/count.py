from gdb_tester import *
import mcgdb

gdb = None

def prepare_and_run(what):
    gdb.set_title(what.replace("do_", ""))
    
    gdb.execute("py import mcgdb; mcgdb.load_by_name('mcgdb-omp')")

    gdb.execute_many(["start"])

    gdb.execute("set what_to_do = {}".format(what))
    gdb.execute("set repeat = 0")
    gdb.execute("set it_count = 1")

    gdb.execute("break finish_data_ready")
    gdb.execute("continue")
    gdb.execute("python import mcgdb")
    
    gdb.save_py_value("str(mcgdb.capture.FunctionBreakpoint.hit_count) + '+' + str( mcgdb.capture.FunctionFinishBreakpoint.hit_count)")
    

def benchmark(_gdb):
    global gdb
    gdb = _gdb

    from . import benchmark
    
    gdb.start(CSource("benchmark.c"), init_hook=mcgdb.testing.gdb__init_mcgdb)
    for what in benchmark.WHAT_TO_RUN:
        gdb.reset(hard=True)
        prepare_and_run(what)
        
    gdb.quit()

    raise FinishException()
