from gdb_tester import *

expected_start = """Enable model GCC OpenMP"""
expected_print = expected_file("opm_seq_print.expected")
expected_print_all = expected_file("opm_seq_print_all.expected")

def test(gdb):

    gdb.start(CSource("parallel-demo.c"))

    gdb.execute("py import mcgdb; mcgdb.load_by_name('mcgdb-omp')")
    gdb.execute_many(["run"])
    
    gdb.execute("opm sequence --print", expected_print, may_fail=True)
    gdb.execute("opm sequence --print --all", expected_print_all, may_fail=True)

    gdb.quit()
