from gdb_tester import *

import mcgdb

def test(gdb):
    gdb.start(CSource("parallel-demo.c"), init_hook=mcgdb.testing.gdb__init_mcgdb)
    
    gdb.execute("py import mcgdb; mcgdb.load_by_name('mcgdb-omp')")
    gdb.execute_many(["start"])
    
    gdb.execute("omp start", "OpenMP parallel zone started")
    gdb.execute("omp step", "in single zone")
    gdb.execute("omp all_out", "Out of the single zone")
    gdb.execute("omp step", "in critical zone")
    gdb.execute("omp critical next", "in critical zone")
    gdb.execute("omp critical next", "in critical zone")
    gdb.execute("omp step out", "outside Critical zone")
    
    gdb.quit()
