struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
struct _IO_FILE;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int sys_nerr;
extern const char *const sys_errlist[];
extern int printf(const char *__restrict __format, ...);
struct  mcc_struct_anon_15
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_15 nanos_smp_args_t;
struct  nanos_args_3_t
{
};
static void smp_ol_main_3(struct nanos_args_3_t *const args);
struct  mcc_struct_anon_11
{
  _Bool mandatory_creation:1;
  _Bool tied:1;
  _Bool clear_chunk:1;
  _Bool reserved0:1;
  _Bool reserved1:1;
  _Bool reserved2:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
};
typedef struct mcc_struct_anon_11 nanos_wd_props_t;
typedef unsigned long int size_t;
struct  nanos_const_wd_definition_tag
{
  nanos_wd_props_t props;
  size_t data_alignment;
  size_t num_copies;
  size_t num_devices;
  size_t num_dimensions;
  const char *description;
};
typedef struct nanos_const_wd_definition_tag nanos_const_wd_definition_t;
struct  mcc_struct_anon_14
{
  void *(*factory)(void *);
  void *arg;
};
typedef struct mcc_struct_anon_14 nanos_device_t;
struct  nanos_const_wd_definition_1
{
  nanos_const_wd_definition_t base;
  nanos_device_t devices[1L];
};
extern void *nanos_smp_factory(void *args);
extern int nanos_omp_get_num_threads_next_parallel(int threads_requested);
typedef void *nanos_team_t;
typedef void *nanos_thread_t;
enum mcc_enum_anon_5
{
  NANOS_OK = 0,
  NANOS_UNKNOWN_ERR = 1,
  NANOS_UNIMPLEMENTED = 2,
  NANOS_ENOMEM = 3,
  NANOS_INVALID_PARAM = 4,
  NANOS_INVALID_REQUEST = 5
};
typedef enum mcc_enum_anon_5 nanos_err_t;
struct mcc_struct_anon_18;
typedef struct mcc_struct_anon_18 nanos_constraint_t;
typedef void *nanos_sched_t;
extern nanos_err_t nanos_create_team(nanos_team_t *team, nanos_sched_t sg, unsigned int *nthreads, nanos_constraint_t *constraints, _Bool reuse, nanos_thread_t *info, nanos_const_wd_definition_t *const_data);
extern void nanos_handle_error(nanos_err_t err);
struct  mcc_struct_anon_12
{
  _Bool is_final:1;
  _Bool is_recover:1;
  _Bool is_implicit:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
  _Bool reserved5:1;
  _Bool reserved6:1;
  _Bool reserved7:1;
};
typedef struct mcc_struct_anon_12 nanos_wd_dyn_flags_t;
struct  mcc_struct_anon_13
{
  nanos_wd_dyn_flags_t flags;
  nanos_thread_t tie_to;
  int priority;
};
typedef struct mcc_struct_anon_13 nanos_wd_dyn_props_t;
typedef void *nanos_wd_t;
struct mcc_struct_anon_4;
typedef struct mcc_struct_anon_4 nanos_copy_data_internal_t;
typedef nanos_copy_data_internal_t nanos_copy_data_t;
struct mcc_struct_anon_0;
typedef struct mcc_struct_anon_0 nanos_region_dimension_internal_t;
typedef void *nanos_wg_t;
extern nanos_err_t nanos_create_wd_compact(nanos_wd_t *wd, nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void **data, nanos_wg_t wg, nanos_copy_data_t **copies, nanos_region_dimension_internal_t **dimensions);
extern nanos_wd_t nanos_current_wd(void);
struct mcc_struct_anon_2;
typedef struct mcc_struct_anon_2 nanos_data_access_internal_t;
typedef nanos_data_access_internal_t nanos_data_access_t;
extern nanos_err_t nanos_submit(nanos_wd_t wd, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_team_t team);
typedef void (*nanos_translate_args_t)(void *, nanos_wd_t);
extern nanos_err_t nanos_create_wd_and_run_compact(nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void *data, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_copy_data_t *copies, nanos_region_dimension_internal_t *dimensions, nanos_translate_args_t translate_args);
struct  mcc_struct_anon_1
{
  _Bool input:1;
  _Bool output:1;
  _Bool can_rename:1;
  _Bool concurrent:1;
  _Bool commutative:1;
};
typedef struct mcc_struct_anon_1 nanos_access_type_internal_t;
typedef long int ptrdiff_t;
struct  mcc_struct_anon_2
{
  void *address;
  nanos_access_type_internal_t flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
extern nanos_err_t nanos_end_team(nanos_team_t team);
int main()
{
  printf("@Base: Let\'s attack the death start !\n");
  printf("@Base: Releash all the starfighters.\n");
  {
    nanos_err_t nanos_err;
    nanos_wd_dyn_props_t dyn_props;
    unsigned int nth_i;
    struct nanos_args_3_t imm_args;
    nanos_data_access_t dependences[1L];
    static nanos_smp_args_t smp_ol_main_3_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_3_t *))&smp_ol_main_3};
    static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 1, .tied = 1, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_3_t), .num_copies = 0, .num_devices = 1, .num_dimensions = 0, .description = "main"}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_main_3_args}}};
    unsigned int nanos_num_threads = nanos_omp_get_num_threads_next_parallel(0);
    nanos_team_t nanos_team = (void *)0;
    nanos_thread_t nanos_team_threads[nanos_num_threads];
    nanos_err = nanos_create_team(&nanos_team, (void *)0, &nanos_num_threads, (nanos_constraint_t *)0, 1, nanos_team_threads, &nanos_wd_const_data.base);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    dyn_props.tie_to = (void *)0;
    dyn_props.priority = 0;
    dyn_props.flags.is_final = 0;
    dyn_props.flags.is_implicit = 1;
    for (nth_i = 1; nth_i < nanos_num_threads; nth_i = nth_i + 1)
      {
        dyn_props.tie_to = nanos_team_threads[nth_i];
        struct nanos_args_3_t *ol_args = 0;
        nanos_wd_t nanos_wd_ = (void *)0;
        nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &dyn_props, sizeof(struct nanos_args_3_t), (void **)&ol_args, nanos_current_wd(), (nanos_copy_data_t **)0, (nanos_region_dimension_internal_t **)0);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
        ;
        nanos_err = nanos_submit(nanos_wd_, 0, (nanos_data_access_t *)0, (void *)0);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
    dyn_props.tie_to = nanos_team_threads[0];
    ;
    nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &dyn_props, sizeof(struct nanos_args_3_t), &imm_args, 0, dependences, (nanos_copy_data_t *)0, (nanos_region_dimension_internal_t *)0, (void (*)(void *, nanos_wd_t))0);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    nanos_err = nanos_end_team(nanos_team);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
  }
  printf("@Base: Everybody\'s back to safety, great job!\n");
}
static void smp_ol_main_0_unpacked(int id)
{
  {
    {
      printf("@%d I cover your backs!\n", id);
    }
  }
}
struct  nanos_args_0_t
{
  int id;
};
static void smp_ol_main_0(struct nanos_args_0_t *const args)
{
  {
    smp_ol_main_0_unpacked((*args).id);
  }
}
static void smp_ol_main_1_unpacked(int id)
{
  {
    {
      printf("@%d I cover your backs as well!\n", id);
    }
  }
}
struct  nanos_args_1_t
{
  int id;
};
static void smp_ol_main_1(struct nanos_args_1_t *const args)
{
  {
    smp_ol_main_1_unpacked((*args).id);
  }
}
struct nanos_ws_desc;
typedef struct nanos_ws_desc nanos_ws_desc_t;
extern nanos_err_t nanos_omp_set_implicit(nanos_wd_t uwd);
typedef void *nanos_ws_item_t;
extern nanos_err_t nanos_worksharing_next_item(nanos_ws_desc_t *wsd, nanos_ws_item_t *wsi);
struct  mcc_struct_anon_10
{
  int lower;
  int upper;
  _Bool execute:1;
  _Bool last:1;
};
typedef struct mcc_struct_anon_10 nanos_ws_item_loop_t;
extern nanos_err_t nanos_omp_barrier(void);
static void smp_ol_main_2_unpacked(nanos_ws_desc_t *wsd_1, int *const id)
{
  int nanos_omp_index_0;
  {
    {
      nanos_err_t nanos_err;
      nanos_err = nanos_omp_set_implicit(nanos_current_wd());
      if (nanos_err != NANOS_OK)
        {
          nanos_handle_error(nanos_err);
        }
    }
    {
      nanos_err_t nanos_err;
      nanos_ws_item_loop_t nanos_item_loop;
      nanos_err = nanos_worksharing_next_item(wsd_1, (void **)&nanos_item_loop);
      if (nanos_err != NANOS_OK)
        {
          nanos_handle_error(nanos_err);
        }
      while (nanos_item_loop.execute)
        {
          for (nanos_omp_index_0 = nanos_item_loop.lower; nanos_omp_index_0 <= nanos_item_loop.upper; nanos_omp_index_0 += 1)
            {
              {
                switch (nanos_omp_index_0)
                    {
                      case 0 :
                      {
                        printf("@%d I attack the energy field!\n", (*id));
                      }
                      break;
                      case 1 :
                      {
                        printf("@%d I attack the generator!\n", (*id));
                      }
                      break;
                      case 2 :
                      {
                        printf("@%d I follow the rabits!\n", (*id));
                      }
                      break;
                    }
              }
            }
          ;
          nanos_err = nanos_worksharing_next_item(wsd_1, (void **)&nanos_item_loop);
        }
    }
    {
      nanos_err_t nanos_err;
      nanos_err = nanos_omp_barrier();
      if (nanos_err != NANOS_OK)
        {
          nanos_handle_error(nanos_err);
        }
    }
  }
}
struct  nanos_args_2_t
{
  nanos_ws_desc_t *wsd_1;
  int *id;
};
static void smp_ol_main_2(struct nanos_args_2_t *const args)
{
  {
    smp_ol_main_2_unpacked((*args).wsd_1, (*args).id);
  }
}
extern nanos_err_t nanos_enter_team(void);
extern int omp_get_thread_num(void);
extern nanos_err_t nanos_omp_single(_Bool *);
struct nanos_lock_t;
typedef struct nanos_lock_t nanos_lock_t;
extern nanos_err_t nanos_set_lock(nanos_lock_t *lock);
enum mcc_enum_anon_4
{
  NANOS_LOCK_FREE = 0,
  NANOS_LOCK_BUSY = 1
};
typedef enum mcc_enum_anon_4 nanos_lock_state_t;
struct  nanos_lock_t
{
  volatile nanos_lock_state_t state_;
};
__attribute__((common)) nanos_lock_t nanos_default_critical_lock;
extern nanos_err_t nanos_unset_lock(nanos_lock_t *lock);
extern nanos_err_t nanos_in_final(_Bool *result);
typedef void *nanos_ws_t;
enum nanos_omp_sched_t
{
  nanos_omp_sched_static = 1,
  nanos_omp_sched_dynamic = 2,
  nanos_omp_sched_guided = 3,
  nanos_omp_sched_auto = 4
};
typedef enum nanos_omp_sched_t nanos_omp_sched_t;
extern nanos_ws_t nanos_omp_find_worksharing(nanos_omp_sched_t kind);
struct  mcc_struct_anon_9
{
  int lower_bound;
  int upper_bound;
  int loop_step;
  int chunk_size;
};
typedef struct mcc_struct_anon_9 nanos_ws_info_loop_t;
typedef void *nanos_ws_info_t;
extern nanos_err_t nanos_worksharing_create(nanos_ws_desc_t **wsd, nanos_ws_t ws, nanos_ws_info_t *info, _Bool *b);
extern nanos_err_t nanos_team_get_num_supporting_threads(int *n);
extern nanos_err_t nanos_malloc(void **p, size_t size, const char *file, int line);
typedef void *nanos_ws_data_t;
struct  nanos_ws_desc
{
  volatile nanos_ws_t ws;
  nanos_ws_data_t data;
  struct nanos_ws_desc *next;
  nanos_thread_t *threads;
  int nths;
};
extern nanos_err_t nanos_team_get_supporting_threads(int *n, nanos_thread_t *list_of_threads);
typedef void *nanos_slicer_t;
extern nanos_slicer_t nanos_find_slicer(const char *slicer);
extern nanos_err_t nanos_create_sliced_wd(nanos_wd_t *uwd, size_t num_devices, nanos_device_t *devices, size_t outline_data_size, int outline_data_align, void **outline_data, nanos_wg_t uwg, nanos_slicer_t slicer, nanos_wd_props_t *props, nanos_wd_dyn_props_t *dyn_props, size_t num_copies, nanos_copy_data_t **copies, size_t num_dimensions, nanos_region_dimension_internal_t **dimensions);
extern nanos_err_t nanos_free(void *p);
extern int nanos_omp_get_thread_num(void);
extern nanos_err_t nanos_leave_team(void);
static void smp_ol_main_3_unpacked(void)
{
  {
    nanos_err_t nanos_err;
    nanos_err = nanos_omp_set_implicit(nanos_current_wd());
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    nanos_err = nanos_enter_team();
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    {
      int id = omp_get_thread_num() + 1;
      printf("@%d On the way to the combat zone.\n", id);
      {
        _Bool single_guard;
        nanos_err_t nanos_err = nanos_omp_single(&single_guard);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
        if (single_guard)
          {
            {
              printf("@%d I opened a breach in the star,\n", id);
              printf("@%d but we can only enter one by one.\n", id);
            }
          }
      }
      {
        nanos_err_t nanos_err;
        nanos_err = nanos_omp_barrier();
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
      {
        nanos_err_t nanos_err;
        nanos_err = nanos_set_lock(&nanos_default_critical_lock);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
        {
          printf("@%d I\'m entering the breach\n", id);
        }
        nanos_err = nanos_unset_lock(&nanos_default_critical_lock);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
      printf("\n@%d Someone needs to cover us!\n", id);
      {
        _Bool single_guard;
        nanos_err_t nanos_err = nanos_omp_single(&single_guard);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
        if (single_guard)
          {
            {
              printf("@%d I give the orders\n", id);
              {
                _Bool mcc_is_in_final;
                nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                if (mcc_err_in_final != NANOS_OK)
                  {
                    nanos_handle_error(mcc_err_in_final);
                  }
                if (mcc_is_in_final)
                  {
                    {
                      printf("@%d I cover your backs!\n", id);
                    }
                  }
                else
                  {
                    {
                      nanos_wd_dyn_props_t nanos_wd_dyn_props;
                      struct nanos_args_0_t *ol_args;
                      nanos_err_t nanos_err;
                      struct nanos_args_0_t imm_args;
                      nanos_data_access_t dependences[1L];
                      static nanos_smp_args_t smp_ol_main_0_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_0_t *))&smp_ol_main_0};
                      static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 1, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_0_t), .num_copies = 0, .num_devices = 1, .num_dimensions = 0, .description = "main"}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_main_0_args}}};
                      nanos_wd_dyn_props.tie_to = 0;
                      nanos_wd_dyn_props.priority = 0;
                      nanos_wd_dyn_props.flags.is_final = 0;
                      nanos_wd_dyn_props.flags.is_implicit = 0;
                      ol_args = (struct nanos_args_0_t *)0;
                      void *nanos_wd_ = (void *)0;
                      nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), (void **)&ol_args, nanos_current_wd(), (nanos_copy_data_t **)0, (nanos_region_dimension_internal_t **)0);
                      if (nanos_err != NANOS_OK)
                        {
                          nanos_handle_error(nanos_err);
                        }
                      if (nanos_wd_ != (void *)0)
                        {
                          (*ol_args).id = id;
                          nanos_err = nanos_submit(nanos_wd_, 0, &dependences[0], (void *)0);
                          if (nanos_err != NANOS_OK)
                            {
                              nanos_handle_error(nanos_err);
                            }
                        }
                      else
                        {
                          imm_args.id = id;
                          nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), &imm_args, 0, &dependences[0], (nanos_copy_data_t *)0, (nanos_region_dimension_internal_t *)0, (void (*)(void *, void *))0);
                          if (nanos_err != NANOS_OK)
                            {
                              nanos_handle_error(nanos_err);
                            }
                        }
                    }
                  }
              }
              {
                _Bool mcc_is_in_final;
                nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                if (mcc_err_in_final != NANOS_OK)
                  {
                    nanos_handle_error(mcc_err_in_final);
                  }
                if (mcc_is_in_final)
                  {
                    {
                      printf("@%d I cover your backs as well!\n", id);
                    }
                  }
                else
                  {
                    {
                      nanos_wd_dyn_props_t nanos_wd_dyn_props;
                      struct nanos_args_1_t *ol_args;
                      nanos_err_t nanos_err;
                      struct nanos_args_1_t imm_args;
                      nanos_data_access_t dependences[1L];
                      static nanos_smp_args_t smp_ol_main_1_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_1_t *))&smp_ol_main_1};
                      static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 1, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_1_t), .num_copies = 0, .num_devices = 1, .num_dimensions = 0, .description = "main"}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_main_1_args}}};
                      nanos_wd_dyn_props.tie_to = 0;
                      nanos_wd_dyn_props.priority = 0;
                      nanos_wd_dyn_props.flags.is_final = 0;
                      nanos_wd_dyn_props.flags.is_implicit = 0;
                      ol_args = (struct nanos_args_1_t *)0;
                      void *nanos_wd_ = (void *)0;
                      nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), (void **)&ol_args, nanos_current_wd(), (nanos_copy_data_t **)0, (nanos_region_dimension_internal_t **)0);
                      if (nanos_err != NANOS_OK)
                        {
                          nanos_handle_error(nanos_err);
                        }
                      if (nanos_wd_ != (void *)0)
                        {
                          (*ol_args).id = id;
                          nanos_err = nanos_submit(nanos_wd_, 0, &dependences[0], (void *)0);
                          if (nanos_err != NANOS_OK)
                            {
                              nanos_handle_error(nanos_err);
                            }
                        }
                      else
                        {
                          imm_args.id = id;
                          nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), &imm_args, 0, &dependences[0], (nanos_copy_data_t *)0, (nanos_region_dimension_internal_t *)0, (void (*)(void *, void *))0);
                          if (nanos_err != NANOS_OK)
                            {
                              nanos_handle_error(nanos_err);
                            }
                        }
                    }
                  }
              }
            }
          }
      }
      {
        nanos_err_t nanos_err;
        nanos_err = nanos_omp_barrier();
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
      {
        int nanos_chunk;
        nanos_ws_info_loop_t nanos_setup_info_loop;
        nanos_err_t nanos_err;
        nanos_ws_desc_t *wsd_1;
        _Bool single_guard;
        struct nanos_args_2_t imm_args;
        void *current_ws_policy = nanos_omp_find_worksharing(nanos_omp_sched_static);
        if (current_ws_policy == 0)
          {
            nanos_handle_error(NANOS_UNIMPLEMENTED);
          }
        nanos_chunk = 1;
        nanos_setup_info_loop.lower_bound = 0;
        nanos_setup_info_loop.upper_bound = 2;
        nanos_setup_info_loop.loop_step = 1;
        nanos_setup_info_loop.chunk_size = nanos_chunk;
        nanos_err = nanos_worksharing_create(&wsd_1, current_ws_policy, (void **)&nanos_setup_info_loop, &single_guard);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
        if (single_guard)
          {
            int sup_threads;
            nanos_err = nanos_team_get_num_supporting_threads(&sup_threads);
            if (nanos_err != NANOS_OK)
              {
                nanos_handle_error(nanos_err);
              }
            if (sup_threads > 0)
              {
                nanos_wd_dyn_props_t dyn_props;
                nanos_err = nanos_malloc((void **)&(*wsd_1).threads, sizeof(void *) * sup_threads, "", 0);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                nanos_err = nanos_team_get_supporting_threads(&(*wsd_1).nths, (*wsd_1).threads);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                struct nanos_args_2_t *ol_args = (struct nanos_args_2_t *)0;
                static nanos_smp_args_t smp_ol_main_2_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_2_t *))&smp_ol_main_2};
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 1, .tied = 1, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_2_t), .num_copies = 0, .num_devices = 1, .num_dimensions = 0, .description = "main"}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_main_2_args}}};
                void *nanos_wd_ = (void *)0;
                dyn_props.tie_to = (void *)0;
                dyn_props.priority = 0;
                dyn_props.flags.is_final = 0;
                dyn_props.flags.is_implicit = 0;
                static void *replicate = (void *)0;
                if (replicate == (void *)0)
                  {
                    replicate = nanos_find_slicer("replicate");
                  }
                if (replicate == (void *)0)
                  {
                    nanos_handle_error(NANOS_UNIMPLEMENTED);
                  }
                nanos_err = nanos_create_sliced_wd(&nanos_wd_, nanos_wd_const_data.base.num_devices, nanos_wd_const_data.devices, (unsigned long int)sizeof(struct nanos_args_2_t), nanos_wd_const_data.base.data_alignment, (void **)&ol_args, (void **)0, replicate, &nanos_wd_const_data.base.props, &dyn_props, 0, (nanos_copy_data_t **)0, 0, (nanos_region_dimension_internal_t **)0);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                (*ol_args).wsd_1 = wsd_1;
                (*ol_args).id = &id;
                nanos_err = nanos_submit(nanos_wd_, 0, (nanos_data_access_t *)0, (void *)0);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                nanos_err = nanos_free((*wsd_1).threads);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
              }
          }
        imm_args.wsd_1 = wsd_1;
        imm_args.id = &id;
        smp_ol_main_2(&(imm_args));
      }
      {
        nanos_err_t nanos_err;
        nanos_err = nanos_omp_barrier();
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
      if (nanos_omp_get_thread_num() == 0)
        {
          {
            printf("@%d Now I attack the inner core...\n", id);
            printf("@%d ... and destroy it !\n", id);
          }
        }
      {
        nanos_err_t nanos_err;
        nanos_err = nanos_omp_barrier();
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
      printf("@%d We\'re done!!!\n", id);
    }
    nanos_err = nanos_omp_barrier();
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    nanos_err = nanos_leave_team();
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
  }
}
static void smp_ol_main_3(struct nanos_args_3_t *const args)
{
  {
    smp_ol_main_3_unpacked();
  }
}
